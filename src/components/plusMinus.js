import { Component } from "react";

class PlusMinus extends Component {
    constructor(props) {
        super(props);

        this.state = {
            number: 0
        }
    }

    onPlusButtonClicked = () => {
        this.setState({
            number: this.state.number + 1
        });
    }

    onMinusButtonClicked = () => {
        this.setState({
            number: this.state.number - 1
        });
    }

    render() {
        return (
            <div style={{ marginLeft: "2rem" }}>
                <p>{this.state.number}</p>
                <div style={{display: "flex"}}>
                    <button style={{marginRight: "1rem"}} onClick={this.onPlusButtonClicked}>Tăng</button>
                    <button onClick={this.onMinusButtonClicked}>Giảm</button>
                </div>
            </div>
        );
    }
}

export default PlusMinus;