import { Component } from "react";

class TimeNow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            time: new Date().toLocaleTimeString(),
            color: "black",
        }
    }

    componentDidMount() {
        this.timer = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    tick() {
        this.setState({
            time: new Date().toLocaleTimeString(),
        });
    }

    onChangeColorButtonClick = () => {
        var seconds = new Date().getSeconds();
        this.setState({
            color: seconds % 2 == 0 ? "red" : "blue"
        });
    }

    render() {
        return (
            <div style={{ marginLeft: "2rem" }}>
                <h3>Hello, world!</h3>
                <p style={{color: this.state.color}}>It is {
                    this.state.time
                }.</p>
                <button onClick={this.onChangeColorButtonClick}>Change color</button>
            </div>
        );
    }
}

export default TimeNow;