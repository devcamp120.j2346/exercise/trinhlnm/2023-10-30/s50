import { Component } from "react";

class LiComponent extends Component {
  render() {
    var { id, title, content } = this.props;
    return (
        <>
            <li>Bước {id}. {title}: {content}</li>
        </>
    );
  }
}

export default LiComponent;