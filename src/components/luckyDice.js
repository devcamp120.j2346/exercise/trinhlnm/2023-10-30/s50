import { Component } from "react";

class LuckyDice extends Component {
    constructor(props) {
        super(props);

        this.state = {
            diceImg: require("../assets/images/dice/dice.png")
        }
    }

    onNemBtnClick = () => {
        var vRandomNumber = Math.floor(Math.random() * 6 + 1);

        switch (vRandomNumber) {
            case 1:
                this.setState({
                    diceImg: require("../assets/images/dice/1.png")
                });
                break;
            case 2:
                this.setState({
                    diceImg: require("../assets/images/dice/2.png")
                });
                break;
            case 3:
                this.setState({
                    diceImg: require("../assets/images/dice/3.png")
                });
                break;
            case 4:
                this.setState({
                    diceImg: require("../assets/images/dice/4.png")
                });
                break;
            case 5:
                this.setState({
                    diceImg: require("../assets/images/dice/5.png")
                });
                break;
            case 6:
                this.setState({
                    diceImg: require("../assets/images/dice/6.png")
                });
                break;
            default:
                this.setState({
                    diceImg: require("../assets/images/dice/dice.png")
                });
                break;
        }
    }

    render() {
        return (
            <div style={{ margin: "2rem" }}>
                <img src={this.state.diceImg} style={{ width: "12%" }} />
                <button style={{ display: "block" }} onClick={this.onNemBtnClick}>Ném</button>
            </div>
        );
    }
}

export default LuckyDice;