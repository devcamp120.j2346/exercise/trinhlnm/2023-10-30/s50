import { Component } from "react";

class TimeClick extends Component {
    constructor(props) {
        super(props);

        this.state = {
            timeList: []
        }
    }

    onAddButtonClick = () => {
        var time = new Date().toLocaleTimeString();
        this.setState({
            timeList: [...this.state.timeList, time]
        });
    }

    render() {
        return (
            <div style={{margin: "2rem"}}>
                <div>List:</div>
                <div>{this.state.timeList.map((e, index) => {
                    return <div>{index + 1}. {e}</div>
                })}</div>
                <button onClick={this.onAddButtonClick}>Add to list</button>
            </div>
        );
    }
}

export default TimeClick;