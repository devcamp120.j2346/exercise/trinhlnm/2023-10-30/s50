import { Component } from "react";

class Animal extends Component {
  render() {
    const kind = this.props.kind;
    return (
      <div>
        {
            kind == "cat" 
                ? <img src={require("../assets/images/cat.jpg")} style={{height: "20rem"}}/> 
                : <p>meow not found :)</p>
        }
      </div>
    );
  }
}

export default Animal;