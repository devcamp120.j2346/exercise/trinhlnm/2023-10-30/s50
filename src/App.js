import Animal from "./components/animal";
import LiComponent from "./components/liComponent";
import LuckyDice from "./components/luckyDice";
import PlusMinus from "./components/plusMinus";
import TimeClick from "./components/timeClick";
import TimeNow from "./components/timeNow";

function App() {
  const steps = [
    {id: 1, title: 'Hello World', content: 'Welcome to learning React!'},
    {id: 2, title: 'Installation', content: 'You can install React from npm.'},
    {id: 3, title: 'Create react app', content: 'Run create-react-app to run project.'},
    {id: 4, title: 'Run init project', content: 'Cd into project and npm start to run project.'},
  ];
  
  return (
    <div>
      {/* <Animal kind="cat"/> */}

      {/* <ul>
        {steps.map((e, index) => {
          return <LiComponent id={e.id} title={e.title} content={e.content}/>
        })}
      </ul> */}

      {/* <PlusMinus/> */}

      {/* <TimeNow/> */}

      {/* <LuckyDice/> */}

      <TimeClick/>
    </div>
  );
}

export default App;
